package com.template.controller;

import com.template.service.GoogleGuavaCacheService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CacheController {

    private static final String MAIN_PAGE_NAME = "index";
    private static final String MAIN_PAGE_ENDPOINT = "/";
    private static final String KFC_ATTRIBUTE_NAME = "kfc";
    private static final String PIZZA_HUT_ATTRIBUTE_NAME = "pizzaHut";
    private static final String MCDONALDS_ATTRIBUTE_NAME = "mcdonalds";
    private static final String PIZZA_MADNESS_ATTRIBUTE_NAME = "pizzaMadness";
    private static final String DUNKIN_DONUTS_ATTRIBUTE_NAME = "dunkinDonuts";
    private static final String BLACK_DELIVERY_ATTRIBUTE_NAME = "blackDelivery";

    private final GoogleGuavaCacheService googleGuavaCacheService;

    public CacheController(GoogleGuavaCacheService googleGuavaCacheService) {
        this.googleGuavaCacheService = googleGuavaCacheService;
    }

    @GetMapping(MAIN_PAGE_ENDPOINT)
    public String getMainPage(Model model) {
        model.addAttribute(PIZZA_HUT_ATTRIBUTE_NAME, googleGuavaCacheService.getRestaurants().get(0));
        model.addAttribute(MCDONALDS_ATTRIBUTE_NAME, googleGuavaCacheService.getRestaurants().get(1));
        model.addAttribute(KFC_ATTRIBUTE_NAME, googleGuavaCacheService.getRestaurants().get(2));
        model.addAttribute(BLACK_DELIVERY_ATTRIBUTE_NAME, googleGuavaCacheService.getRestaurants().get(3));
        model.addAttribute(PIZZA_MADNESS_ATTRIBUTE_NAME, googleGuavaCacheService.getRestaurants().get(4));
        model.addAttribute(DUNKIN_DONUTS_ATTRIBUTE_NAME, googleGuavaCacheService.getRestaurants().get(5));

        return MAIN_PAGE_NAME;
    }
}
