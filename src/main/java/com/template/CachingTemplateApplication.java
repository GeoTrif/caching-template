package com.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class CachingTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(CachingTemplateApplication.class, args);
	}
}
