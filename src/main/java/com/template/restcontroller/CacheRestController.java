package com.template.restcontroller;

import com.template.model.Restaurant;
import com.template.service.GoogleGuavaCacheService;
import com.template.service.HazelcastCacheService;
import com.template.service.SimpleCacheService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CacheRestController {

    private static final String GET_RESTAURANTS_ENDPOINT = "/get-restaurants";

    private final GoogleGuavaCacheService googleGuavaCacheService;
    private final SimpleCacheService simpleCacheService;
    private final HazelcastCacheService hazelcastCacheService;

    public CacheRestController(final GoogleGuavaCacheService googleGuavaCacheService, final SimpleCacheService simpleCacheService, final HazelcastCacheService hazelcastCacheService) {
        this.googleGuavaCacheService = googleGuavaCacheService;
        this.simpleCacheService = simpleCacheService;
        this.hazelcastCacheService = hazelcastCacheService;
    }

    @GetMapping(GET_RESTAURANTS_ENDPOINT)
    public List<Restaurant> getRestaurants() {
        return googleGuavaCacheService.getRestaurants();
    }
}
