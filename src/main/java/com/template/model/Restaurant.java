package com.template.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "restaurant")
public class Restaurant implements Serializable {

    public Restaurant() {
    }

    @Id
    @GeneratedValue
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "owner_name")
    private String ownerName;
    @Column(name = "capacity")
    private int capacity;
    @Column(name = "address")
    private String address;
    @Column(name = "phone_number")
    private String phoneNumber;
}
