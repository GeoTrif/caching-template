package com.template.service;

import com.template.model.Restaurant;
import com.template.repository.RestaurantDao;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
/**
 * This method doesn't work.It won't update the restaurants list.
 *
 * Don't use this kind of caching technique.
 */
public class SimpleCacheService {

    private List<Restaurant> restaurants;

    private final RestaurantDao restaurantDao;

    public SimpleCacheService(RestaurantDao restaurantDao) {
        this.restaurantDao = restaurantDao;
        buildCache();
    }

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    @Scheduled(cron = "${cron.simple.cache}")
    private void buildCache() {
        restaurants = restaurantDao.findAll();
    }
}
