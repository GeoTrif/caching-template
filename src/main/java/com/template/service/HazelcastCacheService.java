package com.template.service;

import com.template.model.Restaurant;
import com.template.repository.RestaurantDao;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HazelcastCacheService {

    private final RestaurantDao restaurantDao;

    public HazelcastCacheService(RestaurantDao restaurantDao) {
        this.restaurantDao = restaurantDao;
    }

    @Cacheable("restaurants")
    public List<Restaurant> getRestaurants() {
        return restaurantDao.findAll();
    }
}
