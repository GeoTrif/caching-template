package com.template.service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.template.model.Restaurant;
import com.template.repository.RestaurantDao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class GoogleGuavaCacheService {

    private LoadingCache<String, List<Restaurant>> cache;

    private RestaurantDao restaurantDao;

    public GoogleGuavaCacheService(RestaurantDao restaurantDao) {
        this.restaurantDao = restaurantDao;
        buildCache();
    }

    public List<Restaurant> getRestaurants() {
        try {
            return cache.get("");
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    private void buildCache() {
        cache = CacheBuilder.newBuilder()
                .expireAfterWrite(1, TimeUnit.MINUTES)
                .maximumSize(1000)
                .build(new CacheLoader<String, List<Restaurant>>() {
                    @Override
                    public List<Restaurant> load(String s) throws Exception {
                        return restaurantDao.findAll();
                    }
                });
    }
}
