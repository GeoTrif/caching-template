INSERT INTO restaurant (id, name, owner_name, capacity, address, phone_number) VALUES (1, "Pizza Hut", "John Doe", 400, "St. Warehouse no. 1", "01234567");
INSERT INTO restaurant (id, name, owner_name, capacity, address, phone_number) VALUES (2, "McDonalds", "John Black", 500, "St. Downtown no. 3", "0987654");
INSERT INTO restaurant (id, name, owner_name, capacity, address, phone_number) VALUES (3, "KFC", "Peter Flint", 600, "St. Uphill no. 34", "65353785");
INSERT INTO restaurant (id, name, owner_name, capacity, address, phone_number) VALUES (4, "Black Delivery", "Jamie Oliver", 700, "St. Around Corner no. 234", "8767466");
INSERT INTO restaurant (id, name, owner_name, capacity, address, phone_number) VALUES (5, "Pizza Madness", "Gordon Ramsay", 800, "St. Somewhere no. 78", "4563777");
INSERT INTO restaurant (id, name, owner_name, capacity, address, phone_number) VALUES (6, "Dunkin Donnuts", "Abe Lincoln", 900, "St. Kansas no. 34", "2346785");
