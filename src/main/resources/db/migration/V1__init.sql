CREATE TABLE IF NOT EXISTS restaurant (
`id` INT NOT NULL,
`name` VARCHAR(45) NOT NULL,
`owner_name` VARCHAR(45) NOT NULL,
`capacity` INT NOT NULL,
`address` VARCHAR(45) NOT NULL,
`phone_number` VARCHAR(45) NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB;